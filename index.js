const $camera = document.getElementById('camera');
const $sentence = document.getElementById('sentence');
const $textSentence = document.getElementById('textSentence');
const $headPerson = document.getElementById('head');
const $audio = document.getElementById('audio');


$headPerson.addEventListener('click', () => {
    if ($sentence.classList.contains('sentence--animation') && $textSentence.classList.contains('text__sentence--animation') ) {
        $sentence.classList.remove('sentence--animation');
        $textSentence.classList.remove('text__sentence--animation');
        if ( !$audio.paused ) $audio.pause();
    } else {
        $sentence.classList.add('sentence--animation');
        $textSentence.classList.add('text__sentence--animation');
        $audio.play()
    }
});

$camera.addEventListener('click', () => {
    if ( $camera.classList.contains('camera--animation') ) {
        $camera.classList.remove('camera--animation');
    } else {
        $camera.classList.add('camera--animation');
    }
});